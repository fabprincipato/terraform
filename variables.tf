variable "provisioning_url" {
    description = "provisioning_url "
    default = "http://paste.debian.net/plain/410320"
}

variable "access_key" {
    description = "AWS access key."
}

variable "secret_key" {
    description = "AWS secret key."
}

variable "region" {
    description = "The AWS region to create things in."
    default = "us-west-2"
}

variable "key_name" {
    description = "Name of the keypair to use in EC2."
    default = "terraform"
}

variable "key_path" {
    descriptoin = "Path to your private key."
    default = "~/.ssh/terraform.pem"
}

variable "key_username" {
    descriptoin = "SSH Username"
    default = "root"
}

variable "ami" {
    description = "AMI"
    default =  "ami-9abea4fb"
}


variable "instance_type" {
    description = "Instance Type"
    default = "t2.micro"
}

variable "serverspec_rep"{
    description = "serverspec repo"
    default = "https://fabprincipato@bitbucket.org/fabprincipato/serverspec.git"
}

