resource "template_file" "host" {
	template = "${file("host.tpl")}"
	vars {
		role = "pastebin"
		policy_url = "https://bitbucket.org/fabprincipato/infra-training2.git"
	}
}


resource "aws_instance" "vm" {
    connection {
        user = "${var.key_username}"
        key_file = "${var.key_path}"
        agent = true
    }
    ami = "${var.ami}"
    instance_type = "${var.instance_type}"
    key_name = "${var.key_name}"
    vpc_security_group_ids = ["${aws_security_group.stikked-security-group.id}"]

    associate_public_ip_address = true

    tags = {
        Name = "XXX"
    }
	
    provisioner "remote-exec" {
        inline = [
	    "cat << FILEXXX > /dev/shm/host.sh",
	    "${template_file.host.rendered}",
	    "FILEXXX",
        ]
    }

    provisioner "remote-exec" {
        inline = [
           "cd /tmp",
           "sudo apt-get clean && sudo apt-get update",
           "wget -O provision.sh ${var.provisioning_url}",
           "sudo chmod a+x provision.sh",
	   "sudo chown root provision.sh",
           "sudo sed -i -e 's/.*ssh-rsa/ssh-rsa/' /root/.ssh/authorized_keys"
        ]
    }
}

resource "null_resource" "provision" {
    depends_on = ["aws_instance.vm"]
    triggers = {
        instance_id = "${aws_instance.vm.id}"
    }
    connection {
	host = "${aws_instance.vm.public_ip}"
        user = "root"
        key_file = "${var.key_path}"
        agent = true
    }
    provisioner "remote-exec" {
        inline = [
           "/tmp/provision.sh"
        ]
    }
}

resource "null_resource" "serverspec" {
    depends_on = ["null_resource.provision"]
    triggers = {
        instance_id = "${aws_instance.vm.id}"
    }
    connection {
	host = "${aws_instance.vm.public_ip}"
        user = "root"
        key_file = "${var.key_path}"
        agent = true
    }
    provisioner "remote-exec" {
        inline = [
	   "apt-get -y install ruby2.0",
	   "gem2.0 install serverspec",
        ]
    }
    #provisioner "remote-exec" {
    #    inline = [
    #       "export test=lol && eval $(echo curl -d text=$lol http://ec2-52-27-245-128.us-west-2.compute.amazonaws.com/api/create)",
    #    ]
    #}
    provisioner "remote-exec" {
        inline = [
	   "cd && rm -rf serverspec && mkdir serverspec && cd serverspec && git clone ${var.serverspec_rep} .",
	   "rake2.0 spec:localhost",
        ]
    }
}

resource "aws_security_group" "stikked-security-group" {
  name = "stikked-security-group"
  description = "Allow port 80 and 22 traffic"

  ingress {
      from_port = 80
      to_port = 80
      protocol = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
      from_port = 22
      to_port = 22
      protocol = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
      from_port = 0
      to_port = 0
      protocol = "-1"
      self = true
  }

  egress {
      from_port = 0
      to_port = 0
      protocol = "-1"
      cidr_blocks = ["0.0.0.0/0"]
  }
}
